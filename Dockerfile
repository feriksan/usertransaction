# Use OpenJDK 11 as the base image
FROM openjdk:21

# Metadata as a label
LABEL maintainer="ferdicool95@gmail.com" version="1.0" description="Spring Transaction Microservice"

# Copy the application JAR into the container
COPY target/assgmentAPI-0.0.1-SNAPSHOT.jar /app/assgmentAPI.jar

# Command to run the application
CMD ["java", "-jar", "/app/assgmentAPI.jar"]