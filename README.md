// README.md
# Test Assesment Backend
### Introduction
Dynamic service transaction API merupakan RESTfullAPI yang di desain untuk melakukan pembayaran ke layanan pembayaran elektronik seperti pulsa, bpjs, dan pam. Program ini mampu menambahkan jenis layanan, user management, user authentication, menyimpan banner, dan melakukan pengecekan pengeluaran dan pemasukan.
### Project Support Features
* Users dapat melakukan registrasi dan login akun
* Authentication menggunakan JWT token
* Manajemen user role
* Transaksi service yang tersedia
* Menambah service dan banner dari sistem
* Melihat income dan expense
* Top up saldo
* Penyimpanan file, untuk gambar banner dan profile picture
* Deploy menggunakan docker
### Installation Guide
* Clone repository [here](https://gitlab.com/feriksan/usertransaction.git).
* Pilih branch master
* Database terkoneksi ke cloud database, jadi tinggal run saja
*     mvn install
* Install Podman for deployment
### Deployment Guide
*     podman build -t assgmentAPI:latest .
*     podman run -d -p 8080:8080 assgmentapi:latest
### Usage
*     mvn spring-boot:run
*     mvn package
*     java -jar target/assgmentAPI-0.0.1-SNAPSHOT.jar
### API Endpoints User
| HTTP Verbs | Endpoints            | Action                       |
|------------|----------------------|------------------------------|
| POST       | /auth/register       | Register user baru           |
| POST       | /api/auth/login      | Login ke user yang sudah ada |
| GET        | /profile             | Melihat profile user         |
| GET        | /expense             | Mengecek Pengeluaran         |
| GET        | /income              | Mengecek Pemasukan           |
| GET        | /balance             | Mengecek balance             |
| PUT        | /profile/update      | Mengedit akun                |
| PUT        | /profile/image       | Mengedit profile picture     |
### Register
    {
        "email":"user@example.com",
        "password":"12345678",
        "firstName":"user",
        "lastName":"name",
        "role": "USERROLE"
    }
### Login
    {
        "email":"user@example.com",
        "password":"12345678"
    }

### API Endpoints Banner
| HTTP Verbs | Endpoints            | Action               |
|------------|----------------------|----------------------|
| POST       | /banner/insert       | Insert Banner        |
| GET        | /banner             | Get all Banner       |
| GET        | /banner/{id}             | Get banner by id     |
| PUT        | /banner/insert/image/{bannerId}      | Mengedit banner image |
| DELETE     | /banner/delete/{id}       | Menghapus Banner     |

### Insert Banner
    {
        "banner_name":"name",
        "banner_image":"imagePath",
        "description":"desc",
    }

### API Endpoints Service
| HTTP Verbs | Endpoints                                    | Action                |
|------------|----------------------------------------------|-----------------------|
| POST       | /service/insert                              | Insert Service        |
| GET        | /service                                     | Get all Service       |
| GET        | /service/{id}                                | Get service by id     |
| PUT        | /service/update/{serviceId}/{column}/{value} | Mengedit data service |
| DELETE     | /service/delete/{id}                         | Menghapus Service     |
### Insert Service
    {
        "service_code":"1",
        "service_name":"Pulsa",
        "service_icon":"pulsaIcon",
        "service_tariff":1000
    }
### API Endpoints Transaction
| HTTP Verbs | Endpoints                                    | Action                          |
|------------|----------------------------------------------|---------------------------------|
| POST       | /transaction                                 | Insert Transaction              |
| POST       | /topup                                       | Top up Balance                  |
| GET        | /transaction/history                                     | Get Transaction History         |
| GET        | /transaction/history/{type}                               | Get Transaction History by Type |
### Insert Transaction
    {
        "transaction":{
            "service_code":3,
            "transaction_type":"PULSA"
        },
        "service_name":"PULSA",
        "invoice_number":"AB12",
        "amount":1000,
        "tariff":2000
    }
### Technologies Used
* [Springboot](https://spring.io/projects/spring-boot) Spring merupakan framework Java yang mempermudah para programmer dalam membuat sebuah aplikasi Java dengan menerapkan salah satunya adalah design-patern : dependency-injection. Spring framework digunakan karena kemudahan dalam pengaturan projek kedepannya, karena segala sesuatunya sudah disediakan oleh Spring Framework.
* [MySQL](https://www.mysql.com/) MySQL adalah sebuah perangkat lunak sistem manajemen basis data SQL atau DBMS yang multialur, multipengguna, dengan sekitar 6 juta instalasi di seluruh dunia
* [Podman](https://podman.io/) Podman adalah container engine yang lengkap untuk menjalankan, mengelola, dan men-debug container yang mematuhi standar Open Container Initiative (OCI). Dengan Podman, para developer dapat mengelola container tanpa menggunakan daemon.
* [Docker](https://www.docker.com/) Docker adalah sekumpulan platform sebagai produk layanan yang menggunakan virtualisasi tingkat OS untuk mengirimkan perangkat lunak dalam paket yang disebut kontainer. Layanan ini memiliki tingkatan gratis dan premium. Perangkat lunak yang menampung kontainer disebut Docker Engine.
### Authors
* [Feriksan](https://gitlab.com/feriksan)