package com.assigment.assgmentapi.controllers;

import com.assigment.assgmentapi.handler.ResponseHandler;
import com.assigment.assgmentapi.models.BannersEntity;
import com.assigment.assgmentapi.models.ServicesEntity;
import com.assigment.assgmentapi.services.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class ServiceController {
    
    private final ServiceService service;
    
    @Autowired
    public ServiceController(ServiceService service){
        this.service = service;
    }
    @GetMapping("/services")
    public ResponseEntity<Object> findAll() {
        List<ServicesEntity> services = service.getAll();
        return ResponseHandler.generateResponse("Sukses", HttpStatus.OK, services);
    }
    @GetMapping("/service/{id}")
    public ResponseEntity<Object> findBanner(@PathVariable("id") int id){
        return ResponseHandler.generateResponse("Sukses", HttpStatus.OK, service.getService(id));
    }
    
    @PostMapping("/service/insert")
    public ResponseEntity<Object> create(@RequestBody ServicesEntity entity){
        return ResponseHandler.generateResponse("Berhasil", HttpStatus.OK, service.insertService(entity));
    }
    
    @PutMapping("/service/update/{serviceId}/{column}/{value}")
    public ResponseEntity<Object> update(@PathVariable("serviceId") int id, @PathVariable("column") String column, @PathVariable("value") String value){
        return ResponseHandler.generateResponse("Berhasil", HttpStatus.OK, service.updateService(id, column, value));
    }
    @DeleteMapping("/service/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") int id){
        return ResponseHandler.generateResponse("Berhasil", HttpStatus.OK, service.deleteService(id));
    }
}
