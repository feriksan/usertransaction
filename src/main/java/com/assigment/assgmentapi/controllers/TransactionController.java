package com.assigment.assgmentapi.controllers;

import com.assigment.assgmentapi.handler.ResponseHandler;
import com.assigment.assgmentapi.models.*;
import com.assigment.assgmentapi.repositories.TransactionRepositories;
import com.assigment.assgmentapi.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class TransactionController {
    
    private final TransactionService service;
    @Autowired
    public TransactionController(TransactionService service){
        this.service = service;
    }
    
    @PostMapping("transaction")
    public ResponseEntity<Object> postTransaction(@RequestBody TransactionDetailsEntity detailsEntity, Principal principal){
        return ResponseHandler.generateResponse("Transaksi Berhasil", HttpStatus.OK, service.insert(detailsEntity,principal));
    }
    
    @PostMapping("topup")
    public ResponseEntity<Object> cekBalance(@RequestBody TransactionDetailsEntity detailsEntity, Principal principal){
        return ResponseHandler.generateResponse("Top up Balance Berhasil", HttpStatus.OK, service.topUp(detailsEntity, principal));
    }
    
    
    @GetMapping("/transaction/history")
    public ResponseEntity<Object> findAll(Principal principal){
        return ResponseHandler.generateResponse("Get History Berhasil", HttpStatus.OK, service.findAll(principal));
    }
    
    @GetMapping("/transaction/history/{type}")
    public ResponseEntity<Object> findAllTopup(@PathVariable("type") String type){
        return ResponseHandler.generateResponse("Get History Berhasil", HttpStatus.OK, service.findByType(type));
    }
}
