package com.assigment.assgmentapi.mapper;

import com.assigment.assgmentapi.models.BannersEntity;
import com.assigment.assgmentapi.models.CashflowEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CashflowRowMapper implements RowMapper<CashflowEntity> {
    @Override
    public CashflowEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        CashflowEntity flow = new CashflowEntity();
        flow.setRecepient(rs.getString("recepient"));
        flow.setSender(rs.getString("sender"));
        flow.setValue(rs.getInt("value"));
        return flow;
    }
}
