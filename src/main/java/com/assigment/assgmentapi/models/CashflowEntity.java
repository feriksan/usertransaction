package com.assigment.assgmentapi.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CashflowEntity {
    @Id
    @GeneratedValue
    private int id;
    private String sender;
    private String recepient;
    private int value;
    
}
