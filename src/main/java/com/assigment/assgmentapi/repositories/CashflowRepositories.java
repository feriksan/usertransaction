package com.assigment.assgmentapi.repositories;

import com.assigment.assgmentapi.mapper.CashflowRowMapper;
import com.assigment.assgmentapi.mapper.ServiceRowMapper;
import com.assigment.assgmentapi.models.CashflowEntity;
import com.assigment.assgmentapi.models.ServicesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CashflowRepositories {
    @Autowired
    JdbcTemplate template;
    public List<CashflowEntity> getAllFlow(){
        List<CashflowEntity> cashflowRowMapperList = new ArrayList<>();
        cashflowRowMapperList.addAll(template.query("SELECT * from cashflow", new CashflowRowMapper()));
        return cashflowRowMapperList;
    }
    
    public String insertFlow(CashflowEntity servicesEntity){
        String queryBanner = "insert into cashflow(sender, recepient, value) values(?, ?, ?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(
                con -> {
                    PreparedStatement ps =
                            con.prepareStatement(queryBanner, new String[]{"id"});
                    ps.setString(1, servicesEntity.getSender());
                    ps.setString(2, servicesEntity.getRecepient());
                    ps.setInt(3, servicesEntity.getValue());
                    
                    return ps;
                },
                keyHolder);
        return "Data Berhasil Di Input";
    }
    
    public CashflowEntity getFlow(int id){
        CashflowEntity cashflowEntity;
        cashflowEntity = template.queryForObject("select * from cashflow where id = ?;", new Object[] { id }, new CashflowRowMapper());
        return cashflowEntity;
    }
}
