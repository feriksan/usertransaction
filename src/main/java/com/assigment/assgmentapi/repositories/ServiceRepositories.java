package com.assigment.assgmentapi.repositories;

import com.assigment.assgmentapi.mapper.ServiceRowMapper;
import com.assigment.assgmentapi.models.ServicesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ServiceRepositories {
    @Autowired
    JdbcTemplate template;
    public List<ServicesEntity> getAllService(){
        List<ServicesEntity> serviceRowMapperList = new ArrayList<>();
        serviceRowMapperList.addAll(template.query("SELECT * from services", new ServiceRowMapper()));
        return serviceRowMapperList;
    }
    
    public String insertService(ServicesEntity servicesEntity){
        String queryBanner = "insert into services(service_code, service_name, service_icon, service_tariff) values(?, ?, ?, ?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(
                con -> {
                    PreparedStatement ps =
                            con.prepareStatement(queryBanner, new String[]{"id"});
                    ps.setString(1, servicesEntity.getService_code());
                    ps.setString(2, servicesEntity.getService_name());
                    ps.setString(3, servicesEntity.getService_icon());
                    ps.setInt(4, servicesEntity.getService_tariff());
                    
                    return ps;
                },
                keyHolder);
        return "Data Berhasil Di Input";
    }
    
    public ServicesEntity getService(int id){
        ServicesEntity servicesEntity;
        servicesEntity = template.queryForObject("select * from services where id = ?;", new Object[] { id }, new ServiceRowMapper());
        return servicesEntity;
    }
    
    public int updateService(int serviceId, String column, String value){
        String queryUpdate =  "update services set " +column+ " = ? where id = ?";
        System.out.println(queryUpdate);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(
                con -> {
                    PreparedStatement ps =
                            con.prepareStatement(queryUpdate, new String[]{"id"});
                    ps.setString(1, value);
                    ps.setInt(2, serviceId);
                    return ps;
                },
                keyHolder);
        return serviceId;
    }
    public String deleteService(int serviceId){
        template.update("delete from services where id = ?;", serviceId);
        return "Ok";
    }
}
