package com.assigment.assgmentapi.services;

import com.assigment.assgmentapi.models.BannersEntity;
import com.assigment.assgmentapi.models.ServicesEntity;
import com.assigment.assgmentapi.repositories.ServiceRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService {
    
    private final ServiceRepositories serviceRepositories;
    @Autowired
    public ServiceService(ServiceRepositories serviceRepositories){
        this.serviceRepositories = serviceRepositories;
    }
    
    public List<ServicesEntity> getAll(){
        return serviceRepositories.getAllService();
    }
    public ServicesEntity getService(int id){
        return serviceRepositories.getService(id);
    }
    public String insertService(ServicesEntity entity){
        return serviceRepositories.insertService(entity);
    }
    public int updateService(int id, String column, String value){
        return serviceRepositories.updateService(id, column, value);
    }
    public String deleteService(int id){
        return serviceRepositories.deleteService(id);
    }
}
