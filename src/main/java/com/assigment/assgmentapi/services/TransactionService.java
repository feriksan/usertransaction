package com.assigment.assgmentapi.services;

import com.assigment.assgmentapi.models.CashflowEntity;
import com.assigment.assgmentapi.models.TransactionDetailsEntity;
import com.assigment.assgmentapi.models.TransactionMapper;
import com.assigment.assgmentapi.models.TransactionsEntity;
import com.assigment.assgmentapi.repositories.CashflowRepositories;
import com.assigment.assgmentapi.repositories.TransactionRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class TransactionService {
    private final TransactionRepositories repositories;
    private final CashflowRepositories cashflowRepositories;
    @Autowired
    public TransactionService(TransactionRepositories transactionRepositories, CashflowRepositories cashflowRepositories){
        this.repositories = transactionRepositories;
        this.cashflowRepositories = cashflowRepositories;
    }
    
    public List<TransactionMapper> findAll(Principal principal){
        List<TransactionMapper> transactionMappers = repositories.getAllTransaction();
        return transactionMappers.stream().filter(s-> Objects.equals(s.getUser(), principal.getName())).collect(Collectors.toList());
    }
    public List<TransactionMapper> findByType(String type){
        List<TransactionMapper> transaction = repositories.getAllTransaction();
        //Java Stream Filter
        return transaction.stream().filter(s->s.getTransaction_type().equals(type)).collect(Collectors.toList());
    }
    
    public Map<String, Object> insert(TransactionDetailsEntity transactionDetails, Principal principal){
        repositories.insertTransaction(transactionDetails, principal.getName());
        Map<String, Object> map = new HashMap<>();
        map.put("invoice_number", transactionDetails.getInvoice_number());
        map.put("service_code", transactionDetails.getTransaction().getService_code());
        map.put("service_name", transactionDetails.getService_name());
        map.put("transaction_type", transactionDetails.getTransaction().getTransaction_type());
        map.put("total_amount", transactionDetails.getTotal_amount());
        map.put("created_on", transactionDetails.getTransaction().getTs());
        insertFlowOut(transactionDetails, principal);
        return map;
    }
    
    public int topUp(TransactionDetailsEntity detailsEntity, Principal principal){
        TransactionsEntity transactions = new TransactionsEntity();
        Instant instant = Instant.now();
        long timeStampMillis = instant.toEpochMilli();
        transactions.setTransaction_type("TOPUP");
        transactions.setUser(principal.getName());
        transactions.setService_code("1");
        detailsEntity.setTransaction(transactions);
        detailsEntity.setInvoice_number("INV" + timeStampMillis + "112");
        detailsEntity.setService_name("TOP UP");
        insertFlowIn(detailsEntity, principal);
        return repositories.insertTransaction(detailsEntity, principal.getName());
    }
    
    public void insertFlowOut(TransactionDetailsEntity transactionDetails, Principal principal){
        CashflowEntity flow = new CashflowEntity();
        flow.setSender(principal.getName());
        flow.setRecepient("System");
        flow.setValue(transactionDetails.getAmount() + transactionDetails.getTariff());
        cashflowRepositories.insertFlow(flow);
    }
    public void insertFlowIn(TransactionDetailsEntity transactionDetails, Principal principal){
        CashflowEntity flow = new CashflowEntity();
        flow.setSender("System");
        flow.setRecepient(principal.getName());
        flow.setValue(transactionDetails.getTotal_amount());
        cashflowRepositories.insertFlow(flow);
    }
}
