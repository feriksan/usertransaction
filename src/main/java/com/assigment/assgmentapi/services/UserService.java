package com.assigment.assgmentapi.services;

import com.assigment.assgmentapi.models.CashflowEntity;
import com.assigment.assgmentapi.models.UsersEntity;
import com.assigment.assgmentapi.repositories.CashflowRepositories;
import com.assigment.assgmentapi.repositories.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepositories userRepositories;
    private final CashflowRepositories cashflowRepositories;
    @Autowired
    public UserService(UserRepositories userRepositories, CashflowRepositories cashflowRepositories){
        this.userRepositories = userRepositories;
        this.cashflowRepositories =cashflowRepositories;
    }
    
    public UsersEntity findById(String email){
        return userRepositories.getUserByUsername(email);
    }
    public Map<String, Object> userExpense(String email){
        List<CashflowEntity> getUserCashFlow = cashflowRepositories.getAllFlow();
        //Java stream mapToInt
        Integer expense = getUserCashFlow.stream().filter(s-> Objects.equals(s.getSender(), email)).mapToInt(CashflowEntity::getValue).sum();
        Map<String, Object> userExpense = new HashMap<>();
        userExpense.put("User", email);
        userExpense.put("expense", expense);
        return userExpense;
    }
    public Map<String, Object> userIncome(String email){
        List<CashflowEntity> getUserCashFlow = cashflowRepositories.getAllFlow();
        //Java stream mapToInt
        Integer expense = getUserCashFlow.stream().filter(s-> Objects.equals(s.getRecepient(), email)).mapToInt(CashflowEntity::getValue).sum();
        Map<String, Object> userExpense = new HashMap<>();
        userExpense.put("User", email);
        userExpense.put("income", expense);
        return userExpense;
    }
    
    
    public UsersEntity update(UsersEntity users, String email){
        UsersEntity exsistingUser = userRepositories.getUserByUsername(email);
        exsistingUser.setFirstName(users.getFirstName());
        exsistingUser.setLastName(users.getLastName());
        return userRepositories.insertUser(exsistingUser);
    }
    
    public UsersEntity updateProfilePicture(UsersEntity users, String email){
        UsersEntity exsistingUser = userRepositories.getUserByUsername(email);
        exsistingUser.setProfile_image(users.getProfile_image());
        return userRepositories.updateProfilePict(exsistingUser, email);
    }
}
